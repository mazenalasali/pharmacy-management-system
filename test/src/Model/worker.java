/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Date;

/**
 *
 * @author user
 */
public class worker {
    
    private int id;
    private String name;
    private String surname;
    private String address;
    private String date;
    private String email;
    private String nr_cel;
    private String status;
    private String username;
    private String pass;
    private String type;
    private String shenime;
    private Date created_at;
    
    public worker()
    {
        id = 0;
       name = null;
       surname = null;
       address = null;
       date = null;
       email = null;
       nr_cel = null;
       username = null;
       pass = null;
       status = null;
       shenime = null;
    }
    
    
	public worker(int id, String name, String surname, String address, String date, String email, String nr_cel,
			String status, String username, String type, String shenime, Date created_at) {
		super();
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.address = address;
		this.date = date;
		this.email = email;
		this.nr_cel = nr_cel;
		this.status = status;
		this.username = username;
		this.pass = pass;
		this.type = type;
		this.shenime = shenime;
		this.created_at = created_at;
	}


	public worker(String n,String s,String a,String  date,String em,String cel,String user,String pass,String st,String shenim)
    {
        this.name = n;
        this.surname = s;
        this.address = a;
        this.date = date;
        this.email = em;
        this.nr_cel = cel;
        this.username = user;
        this.pass = pass;
        this.status = st;
        this.shenime = shenim;
    }
      public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAge() {
        return date;
    }

    public void setAge(String date) {
        this.date = date;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNr_cel() {
        return nr_cel;
    }

    public void setNr_cel(String nr_cel) {
        this.nr_cel = nr_cel;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public String getShenime() {
		return shenime;
	}
	public void setShenime(String shenime) {
		this.shenime = shenime;
	}


	public String getDate() {
		return date;
	}


	public void setDate(String date) {
		this.date = date;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public Date getCreated_at() {
		return created_at;
	}


	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}
	
     
}
