/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author user
 */
public class presctiption {
    
    private int id;
    private client client;
    private String note;
    private String medicine;
    private String Doktor;
    private String spitali;

    

    public presctiption() {
        id = 0;
        client = new client();
        note = null;
        medicine = null;
        Doktor = null;
        spitali = null;
    }

    public presctiption(int id, Model.client client, String note, String medicine, String dok, String spit) {
		super();
		this.id = id;
		this.client = client;
		this.note = note;
		this.medicine = medicine;
		this.Doktor = dok;
		this.spitali = spit;
	}

	public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public client getClient() {
		return client;
	}

	public void setClient(client client) {
		this.client = client;
	}

	public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getMedicine() {
        return medicine;
    }

    public void setMedicine(String medicine) {
        this.medicine = medicine;
    }

	public String getDoktor() {
		return Doktor;
	}

	public void setDoktor(String doktor) {
		Doktor = doktor;
	}

	public String getSpitali() {
		return spitali;
	}

	public void setSpitali(String spitali) {
		this.spitali = spitali;
	}
    
}
