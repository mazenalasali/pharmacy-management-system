/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author user
 */
public class Invoice {
    
    private int id ;
    private int artikull;
    private int fatura;
    private int klienti;
    private int sasia;
    private double cmimi;
    private double totali;
	public Invoice(int id, int artikull, int fatura, int klienti, int sasia, double cmimi, double totali) {
		super();
		this.id = id;
		this.artikull = artikull;
		this.fatura = fatura;
		this.klienti = klienti;
		this.sasia = sasia;
		this.cmimi = cmimi;
		this.totali = totali;
	}
	
	
	
	public Invoice() {
		super();
		
	}



	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getArtikull() {
		return artikull;
	}
	public void setArtikull(int artikull) {
		this.artikull = artikull;
	}
	public int getFatura() {
		return fatura;
	}
	public void setFatura(int fatura) {
		this.fatura = fatura;
	}
	public int getKlienti() {
		return klienti;
	}
	public void setKlienti(int klienti) {
		this.klienti = klienti;
	}
	public int getSasia() {
		return sasia;
	}
	public void setSasia(int sasia) {
		this.sasia = sasia;
	}
	public double getCmimi() {
		return cmimi;
	}
	public void setCmimi(double cmimi) {
		this.cmimi = cmimi;
	}
	public double getTotali() {
		return totali;
	}
	public void setTotali(double totali) {
		this.totali = totali;
	}

    
    
}
