package Controller;

import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import Model.article;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Artikull_PDF {

	
	public static void writeRaport() throws IOException{
		
		Document document = new Document(PageSize.A4.rotate());
		Database db = new Database();
		try {
			
			PdfWriter.getInstance(document, new FileOutputStream("ArtikullReport.pdf"));
			document.open();
			document.add(new Paragraph("Raport mbi Artikullin", FontFactory.getFont(FontFactory.HELVETICA, 6)));
			document.add(new Paragraph(new Date().toString()));
			document.add(new Paragraph("\n"));
			PdfPTable table = new PdfPTable(7);
			PdfPCell id = new PdfPCell(new Paragraph("Id"));
			//id.setColspan(1);
			id.setHorizontalAlignment(Element.ALIGN_LEFT);
			id.setBackgroundColor(BaseColor.LIGHT_GRAY);
			table.addCell(id);
			PdfPCell kodi = new PdfPCell(new Paragraph("Kodi"));
			//kodi.setColspan(3);
			kodi.setHorizontalAlignment(Element.ALIGN_LEFT);
			kodi.setBackgroundColor(BaseColor.LIGHT_GRAY);
			table.addCell(kodi);
			PdfPCell pershkrimi = new PdfPCell(new Paragraph("Pershkrimi"));
			//pershkrimi.setColspan(5);
			pershkrimi.setHorizontalAlignment(Element.ALIGN_LEFT);
			pershkrimi.setBackgroundColor(BaseColor.LIGHT_GRAY);
			table.addCell(pershkrimi);
			PdfPCell njesia = new PdfPCell(new Paragraph("Njesia"));
			//njesia.setColspan(1);
			njesia.setHorizontalAlignment(Element.ALIGN_LEFT);
			njesia.setBackgroundColor(BaseColor.LIGHT_GRAY);
			table.addCell(njesia);
			PdfPCell sasia = new PdfPCell(new Paragraph("Sasia"));
			//sasia.setColspan(1);
			sasia.setHorizontalAlignment(Element.ALIGN_CENTER);
			sasia.setBackgroundColor(BaseColor.LIGHT_GRAY);
			table.addCell(sasia);
			PdfPCell cmimi_blerje = new PdfPCell(new Paragraph("Cmimi i Shitjes"));
			
			//cmimi_blerje.setColspan(2);
			cmimi_blerje.setHorizontalAlignment(Element.ALIGN_LEFT);
			cmimi_blerje.setBackgroundColor(BaseColor.LIGHT_GRAY);
			table.addCell(cmimi_blerje);
			PdfPCell totali = new PdfPCell(new Paragraph("Totali"));
			//totali.setColspan(2);
			totali.setHorizontalAlignment(Element.ALIGN_LEFT);
			totali.setBackgroundColor(BaseColor.LIGHT_GRAY);
			table.addCell(totali);
			
			String sql = "select articles.id as id, articles.kodi_1 as kodi,description, qty,Njesia as njesia, sum(shitje.sasia) as sasia, price_out,sum(totali) as totali from shitje inner join articles on `artikull_id` = articles.id inner join fatura on fatura_id= fatura.id where likujdim = 'GJENERUAR' group by (`artikull_id`)";
			Connection conn = db.connect();
			Statement st;
			ResultSet rs;
			ObservableList<article> articles = FXCollections.observableArrayList();
			try{
			st = conn.createStatement();
			rs = st.executeQuery(sql);
		
			article a;
			while(rs.next())
			{
				
				a = new article();
				a.setId(rs.getInt("id"));
				a.setKodi1(rs.getString("kodi"));
				a.setPershkrimi(rs.getString("description"));
				a.setNjesia(rs.getString("njesia"));
				a.setSasia(rs.getInt("sasia"));
				a.setPriceout(rs.getDouble("price_out"));
				a.setTotali_artikull(rs.getDouble("totali"));
				articles.add(a);
			}
			}catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
			for(int i = 0; i < articles.size(); i++)
			{
				article a = articles.get(i);
				table.addCell(Integer.toString(a.getId()));
				table.addCell(a.getKodi1());
				table.addCell(a.getPershkrimi());
				table.addCell(a.getNjesia());
				table.addCell(Double.toString(a.getSasia()));
				table.addCell(Double.toString(a.getPriceout()));
				table.addCell(Double.toString(a.getTotali_artikull()));
			}
			
			
			   float[] columnWidths = {1, 3, 3,2,2,2,2};
			 table.setWidths(columnWidths);
			document.add(table);
			
			document.close();
			Desktop.getDesktop().open(new File("ArtikullReport.pdf"));
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public static void main(String [] args) throws IOException
	{
		writeRaport();
	}
	
}
