package Controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.controlsfx.dialog.Dialogs;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Login implements Initializable{
	
	Database db = new Database();
	@FXML
	private AnchorPane anchorpane;
	@FXML
	private TextField UsernameField;
	@FXML
	private TextField PasswordField;
	@FXML
	private Label userLabel;
	@FXML
	private Label passLabel;
	@FXML
	private Label news;
	@FXML
	private Button btnLogin;
	
	@FXML
	private void btnLoginClicked(ActionEvent event)
	{
		String sql = "select * from users where username='"+UsernameField.getText()+"' and " + "password='"+PasswordField.getText()+"'";
		Statement st = null;
		ResultSet rs = null;
		Connection conn = db.connect();
		
		
		try{
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			
			if(rs.next())
			{
				System.out.println(rs.getString(8) +" " + rs.getString(2));
				if(rs.getString(11).equals("ADMIN")){
				
				Stage primaryStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/View/MainPage.fxml").openStream());
				mainController maincontroller = (mainController)loader.getController();
				maincontroller.getUser(rs.getInt(1), rs.getString(2),rs.getString(3));
				Scene scene = new Scene(root,1102,677);
				primaryStage.setScene(scene);
				primaryStage.show();
				close();
				}
				else if(rs.getString(11).equals("WORKER")){
					Stage primaryStage = new Stage();
					FXMLLoader loader = new FXMLLoader();
					Pane root = loader.load(getClass().getResource("/View/POS_System.fxml").openStream());
					posSystemController maincontroller = (posSystemController)loader.getController();
					maincontroller.getUser(rs.getInt(1), rs.getString(2),rs.getString(3));
					Scene scene = new Scene(root,1500,900);
					primaryStage.setScene(scene);
					primaryStage.show();
					close();
				
					
				}
			}else
			{
				Dialogs.create()
				.title("MESAZHI")
				.message("Username ose passwordi nuk eshte i sakte !!! ")
				.styleClass(org.controlsfx.dialog.Dialog.STYLE_CLASS_NATIVE)
				.showError();
			}
				
			
			
		}catch(SQLException ex){
			Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	private void close()
	{
		Stage stage = (Stage) btnLogin.getScene().getWindow();
		
		stage.close();
		
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}

}
