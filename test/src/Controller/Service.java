package Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Model.Invoice;
import Model.article;
import Model.fornitor;
import Model.worker;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;

public class Service {
	public String namesurname;
	public int id;
	public String kategori;
	Database db = new Database();
	public Service(){
		
	}
	
	public ResultSet getcategories()
	{
		String sql = "select * from categories where status='AKTIV'";
		Connection conn = db.connect();
		Statement st = null;
		ResultSet rs = null;
		
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return rs;
	}
	public ObservableList<String> getNjesia()
	{
		ObservableList<String> njesia = FXCollections.observableArrayList();
		njesia.add("COPE");
		njesia.add("KOKERR");
		njesia.add("KG");
		return njesia;
		
	}
	public int getkategori(String str)
	{
		String sql = "select * from categories where name='"+str+"' and status='AKTIV'";
		Connection conn = db.connect();
		Statement st = null;
		ResultSet rs = null;
		int value = 0;
		
		
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			if(rs.next())
			{
				value = rs.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return value;
	}
	public int getworker(String str)
	{
		String sql = "select * from users where name='"+str+"' and status='AKTIV'";
		Connection conn = db.connect();
		Statement st = null;
		ResultSet rs = null;
		int value = 0;
		
		
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			if(rs.next())
			{
				value = rs.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return value;
	}
	public ObservableList<String> getWorkers()
	{
		String sql = "select * from users where type='WORKER' and status='AKTIV'";
		ObservableList<String> workers = FXCollections.observableArrayList();
		Connection conn = db.connect();
		Statement st = null;
		ResultSet rs = null;
		worker worker;
		
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			while(rs.next())
			{
				
				workers.add(rs.getString("name"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return workers;
	}
	public ObservableList<String> getFornitor()
	{
		String sql = "select * from furnitors where   status='AKTIV'";
		ObservableList<String> workers = FXCollections.observableArrayList();
		Connection conn = db.connect();
		Statement st = null;
		ResultSet rs = null;
		
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			while(rs.next())
			{
				workers.add(rs.getString("name"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return workers;
	}
	public int getfurnitors(String str)
	{
		String sql = "select * from furnitors where name='"+str+"' and status='AKTIV'";
		Connection conn = db.connect();
		Statement st = null;
		ResultSet rs = null;
		int value = 0;
		
		
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			if(rs.next())
			{
				value = rs.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return value;
	}
	public ObservableList<String> getPershkrimi()
	{
		ObservableList<String> pershkrimi = FXCollections.observableArrayList();
		String sql = "select * from articles";
		Connection conn = db.connect();
		Statement st = null;
		ResultSet rs = null;
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			if(rs.next())
			{
				pershkrimi.add(rs.getString(4));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return pershkrimi;
		
	}
	public ObservableList<String> getWorker()
	{
		ObservableList<String> pershkrimi = FXCollections.observableArrayList();
		String sql = "select * from users ";
		Connection conn = db.connect();
		Statement st = null;
		ResultSet rs = null;
		worker w;
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			while(rs.next())
			{
				
				pershkrimi.add(rs.getString(2));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return pershkrimi;
		
	}
	
	public String getkategoria(int id)
	{
		String sql = "select * from categories where id='"+id+"'";
		Connection conn = db.connect();
		Statement st = null;
		ResultSet rs = null;
		String value = null;
		
		
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			if(rs.next())
			{
				value = rs.getString(2);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return value;
	}
	public boolean findKlient(String name)
	{
		
		String sql = "select * from clients where name='"+name+"' or surname='"+name+"'";
		Connection conn = db.connect();
		Statement st = null;
		ResultSet rs = null;
		String value = null;
		boolean found = false;
		
		
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			while(rs.next())
			{
				id = rs.getInt(1);
				namesurname = rs.getString(2) + " " + rs.getString(3);
				kategori = getkategoria(rs.getInt(7));
				found = true;
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	public int getShitje(int id)
	{
		String sql = "select * from gjenerim_fature where article_id='"+id+"'";
		Connection conn = db.connect();
		Statement st = null;
		ResultSet rs = null;
		int count = 0 ;
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			while(rs.next())
			{
				count = rs.getInt(4);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return count;
	}
	public ResultSet getArticles()
	{
		String sql = "select * from articles where status='AKTIV'";
		Connection conn = db.connect();
		Statement st = null;
		ResultSet rs = null;
		
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return rs;
	}
	public ObservableList<Invoice> getFatura(int id)
	{
		String sql = "select * from shitje where fatura_id='"+id+"'";
		ObservableList<Invoice> invoice = FXCollections.observableArrayList();
		Connection conn = db.connect();
		Statement st = null;
		ResultSet rs = null;
		int count = 0 ;
		Invoice in;
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			while(rs.next())
			{
				 in = new Invoice(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getInt(4), rs.getInt(5), rs.getDouble(6),rs.getDouble(7));
				invoice.add(in);
			
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return invoice;
	}
	public article getArtikuj(int id,int sasia,double tot)
	{
		String sql = "select * from articles where id='"+id+"'";
		ObservableList<article> invoice = FXCollections.observableArrayList();
		Connection conn = db.connect();
		Statement st = null;
		ResultSet rs = null;
		int count = 0 ;
		article in = new article();
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			while(rs.next())
			{
				 in.setId(rs.getInt(1));
				 in.setKodi1(rs.getString(2));
				 in.setPershkrimi(rs.getString(4));
				 in.setNjesia(rs.getString(5));
				 in.setSasia(sasia);
				 in.setTotali_artikull(tot);
				 in.setPriceout(rs.getDouble(12));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return in;
	}
	
	
	//numeric values Validation
			public EventHandler<KeyEvent> numeric_Validation(final Integer max_Lengh) {
			    return new EventHandler<KeyEvent>() {
			        @Override
			        public void handle(KeyEvent e) {
			            TextField txt_TextField = (TextField) e.getSource();                
			            if (txt_TextField.getText().length() >= max_Lengh) {                    
			                e.consume();
			            }
			            if(e.getCharacter().matches("[0-9.]")){ 
			                if(txt_TextField.getText().contains(".") && e.getCharacter().matches("[.]")){
			                    e.consume();
			                }else if(txt_TextField.getText().length() == 0 && e.getCharacter().matches("[.]")){
			                    e.consume(); 
			                }
			            }else{
			                e.consume();
			            }
			        }
			    };
			}    
			//letter validation
			public EventHandler<KeyEvent> letter_Validation(final Integer max_Lengh) {
			    return new EventHandler<KeyEvent>() {
			        @Override
			        public void handle(KeyEvent e) {
			            TextField txt_TextField = (TextField) e.getSource();                
			            if (txt_TextField.getText().length() >= max_Lengh) {                    
			                e.consume();
			            }
			            if(e.getCharacter().matches("[A-Za-z]")){ 
			            }else{
			                e.consume();
			            }
			        }
			    };
			}    

}
