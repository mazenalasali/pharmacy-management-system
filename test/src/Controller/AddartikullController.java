package Controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import org.controlsfx.dialog.Dialogs;

import Model.article;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class AddartikullController implements Initializable{

	Database db = new Database();
	Service service = new Service();
	
	ObservableList<String> njesialist = FXCollections.observableArrayList("KG","COPE","KOKERR");
	
	@FXML
	private TextField kodi1,kodi2,pershkrimi, min, cmimi;
	
	@FXML
	private ChoiceBox njesia, kategoria;
	
	@FXML
	private CheckBox status, kompesim;
	
	@FXML
	private Button save;
	
	//hello from me saadasd
	
	@FXML
	public void addartikull(ActionEvent event)
	{
		 String sql = "INSERT INTO `articles`(`id`, `kodi_1`, `kodi_2`, `description`, `Njesia`,`min`,`kategori_id`, `kompesim`, `qty`, `status`, `price_in`,`price_out`)"+ " VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
		 Connection conn = db.connect();
		 int kat_id = 1;
		 String kat="";
		 String njesi = "";
		 if(njesia.isPressed())
		 njesi = njesia.getSelectionModel().getSelectedItem().toString();
		 else
			 njesi = "COPE";
		 
		 if(kategoria.isPressed()){
		  kat = kategoria.getSelectionModel().getSelectedItem().toString();
		 kat_id = service.getkategori(kat);
		 System.out.println(kat_id);
		 }
		 String st="",kom="";
		 
		 if(status.isSelected())
		 {
			 st = "AKTIV";
		 }
		 else
			 st = "PASIV";
		 
		 if(kompesim.isSelected()) kom = "PO";
		 else kom = "JO";
		 
		 if(kodi1.getText().isEmpty() ||pershkrimi.getText().isEmpty()|| min.getText().isEmpty() || cmimi.getText().isEmpty())
			{
				save.disabledProperty();
				Dialogs.create()
				.title("MESAZHI")
				.message("Ju lutem plotesojini te gjitha fushat !!! ")
				.styleClass(org.controlsfx.dialog.Dialog.STYLE_CLASS_NATIVE)
				.showWarning();
				return;
			}
		 
		 article a = new article(0,kodi1.getText(),kodi2.getText(),pershkrimi.getText(),njesi,Integer.parseInt(min.getText()),kat,kom,0,st, Double.parseDouble(cmimi.getText()),0.0);
		
		 try{
			 PreparedStatement pst = (PreparedStatement)conn.prepareStatement(sql);
			 int id = 0;
			 pst.setInt(1, id);
			 pst.setString(2, a.getKodi1());
			 pst.setString(3, a.getKodi2());
			 pst.setString(4, a.getPershkrimi());
			 pst.setString(5, a.getNjesia());
			 pst.setInt(6, a.getMin());
			 pst.setInt(7, kat_id);
			 pst.setString(8, a.getKompesim());
			 pst.setInt(9, a.getQuantity());
			 pst.setString(10,a.getStatus());
			 pst.setDouble(11,a.getPricein());
			 pst.setDouble(12, a.getPriceout());
			 
			 pst.execute();
			 pst.close();
			 conn.close();
			 Dialogs.create()
				.title("MESAZHI")
				.message("Artikulli u ruajt me sukses !!! ")
				.styleClass(org.controlsfx.dialog.Dialog.STYLE_CLASS_NATIVE)
				.showInformation();
		 }catch(SQLException ex) {
	            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
		 }
		 close();
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		njesia.setItems(njesialist);
		ResultSet rs = service.getcategories();
		ObservableList<String> list = FXCollections.observableArrayList();
		try {
			while(rs.next())
			{
				list.add(rs.getString(2));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		kategoria.setItems(list);
		njesia.getSelectionModel().select(1);
		min.addEventFilter(KeyEvent.KEY_TYPED, service.numeric_Validation(10));
		cmimi.addEventFilter(KeyEvent.KEY_TYPED, service.numeric_Validation(100));
		kodi1.addEventFilter(KeyEvent.KEY_TYPED, service.numeric_Validation(20));
		kodi2.addEventFilter(KeyEvent.KEY_TYPED, service.numeric_Validation(20));
		
		
		
	}
	private void close()
	{
		Stage stage = (Stage) save.getScene().getWindow();
		
		stage.close();
	}
	

}
