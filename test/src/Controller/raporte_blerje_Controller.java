package Controller;

import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.Date;
import java.util.ResourceBundle;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import Model.article;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class raporte_blerje_Controller implements Initializable{

	Database db = new Database();
	Service service = new Service();
	
	@FXML
	private DatePicker nga,deri;
	
	@FXML
	private Button mbyll, gjenero;
	
	@FXML
	public void mbyllAction(ActionEvent event)
	{
		Stage stage = (Stage) mbyll.getScene().getWindow();
		stage.close();
	}
	@FXML
	public void gjeneroAction(ActionEvent event)
	{
		Document document = new Document(PageSize.A4.rotate());
		Database db = new Database();
		LocalDate ld1 = nga.getValue();
		LocalDate ld2 = deri.getValue();
		Date date1 = java.sql.Date.valueOf(ld1);
		Date date2 = java.sql.Date.valueOf(ld2);
		try {
			
			PdfWriter.getInstance(document, new FileOutputStream("Raporte/BlerjeRaport"+date1 +" - "+date2 + ".pdf"));
			document.open();
			document.add(new Paragraph("Raport mbi Artikullin", FontFactory.getFont(FontFactory.HELVETICA, 6)));
			document.add(new Paragraph(new Date().toString()));
			document.add(new Paragraph("\n"));
			PdfPTable table = new PdfPTable(7);
			PdfPCell id = new PdfPCell(new Paragraph("Id"));
			//id.setColspan(1);
			id.setHorizontalAlignment(Element.ALIGN_LEFT);
			id.setBackgroundColor(BaseColor.LIGHT_GRAY);
			table.addCell(id);
			PdfPCell kodi = new PdfPCell(new Paragraph("Kodi"));
			//kodi.setColspan(3);
			kodi.setHorizontalAlignment(Element.ALIGN_LEFT);
			kodi.setBackgroundColor(BaseColor.LIGHT_GRAY);
			table.addCell(kodi);
			PdfPCell pershkrimi = new PdfPCell(new Paragraph("Pershkrimi"));
			//pershkrimi.setColspan(5);
			pershkrimi.setHorizontalAlignment(Element.ALIGN_LEFT);
			pershkrimi.setBackgroundColor(BaseColor.LIGHT_GRAY);
			table.addCell(pershkrimi);
			PdfPCell njesia = new PdfPCell(new Paragraph("Njesia"));
			//njesia.setColspan(1);
			njesia.setHorizontalAlignment(Element.ALIGN_LEFT);
			njesia.setBackgroundColor(BaseColor.LIGHT_GRAY);
			table.addCell(njesia);
			PdfPCell sasia = new PdfPCell(new Paragraph("Sasia"));
			//sasia.setColspan(1);
			sasia.setHorizontalAlignment(Element.ALIGN_CENTER);
			sasia.setBackgroundColor(BaseColor.LIGHT_GRAY);
			table.addCell(sasia);
			PdfPCell cmimi_blerje = new PdfPCell(new Paragraph("Cmimi i Shitjes"));
			
			//cmimi_blerje.setColspan(2);
			cmimi_blerje.setHorizontalAlignment(Element.ALIGN_LEFT);
			cmimi_blerje.setBackgroundColor(BaseColor.LIGHT_GRAY);
			table.addCell(cmimi_blerje);
			PdfPCell totali = new PdfPCell(new Paragraph("Totali"));
			//totali.setColspan(2);
			totali.setHorizontalAlignment(Element.ALIGN_LEFT);
			totali.setBackgroundColor(BaseColor.LIGHT_GRAY);
			table.addCell(totali);
			
			String sql = "select articles.id as id, articles.kodi_1 as kodi,description, qty,Njesia as njesia, sum(blerje.sasia) as sasia, price_in,sum(totali) as totali from blerje inner join articles on `article_id` = articles.id inner join fatura on fatura_id= fatura.id where create_at between '"+ date1 +"' and '" + date2 +"' group by (`article_id`)";
			Connection conn = db.connect();
			Statement st;
			ResultSet rs;
			ObservableList<article> articles = FXCollections.observableArrayList();
			try{
			st = conn.createStatement();
			rs = st.executeQuery(sql);
		
			article a;
			while(rs.next())
			{
				
				a = new article();
				a.setId(rs.getInt("id"));
				a.setKodi1(rs.getString("kodi"));
				a.setPershkrimi(rs.getString("description"));
				a.setNjesia(rs.getString("njesia"));
				a.setSasia(rs.getInt("sasia"));
				a.setPriceout(rs.getDouble("price_out"));
				a.setTotali_artikull(rs.getDouble("totali"));
				articles.add(a);
			}
			}catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
			for(int i = 0; i < articles.size(); i++)
			{
				article a = articles.get(i);
				table.addCell(Integer.toString(a.getId()));
				table.addCell(a.getKodi1());
				table.addCell(a.getPershkrimi());
				table.addCell(a.getNjesia());
				table.addCell(Double.toString(a.getSasia()));
				table.addCell(Double.toString(a.getPriceout()));
				table.addCell(Double.toString(a.getTotali_artikull()));
			}
			
			
			   float[] columnWidths = {1, 3, 3,2,2,2,2};
			 table.setWidths(columnWidths);
			document.add(table);
			
			document.close();
			Desktop.getDesktop().open(new File("Raporte/BlerjeRaport"+date1 +" - "+date2 + ".pdf"));
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}

}
