package Controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

import Model.article;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class Artikuj_Minimum_Controller implements Initializable{
	
	Database db = new Database();
	
	@FXML
	private TableView<article> table;
	
	@FXML
	private TableColumn <article, Integer >id;
	
	@FXML
	private TableColumn <article, String >kodi;
	
	@FXML
	private TableColumn <article, String >pershkrimi;
	
	@FXML
	private TableColumn <article, String >njesi;
	
	@FXML
	private TableColumn <article, Integer >min;
	
	@FXML
	private TableColumn <article, Double >cmimi;
	
	@FXML
	private TableColumn <article, Double >gjendje;
	
	public ObservableList<article> getArticles()
	{
		ObservableList<article> articles = FXCollections.observableArrayList();
		Connection conn = db.connect();
		Statement st = null;
		ResultSet rs = null;
		article a;
		String sql = "select * from articles where qty < min";
		
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			
			while(rs.next())
			{
				a = new article(rs.getInt(1),rs.getString(2),rs.getString(4),rs.getString(5),rs.getInt(6),rs.getDouble(11),rs.getInt(9));
				articles.add(a);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return articles;
	}
	
	
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		id.setCellValueFactory(new PropertyValueFactory<article, Integer>("id"));
		kodi.setCellValueFactory(new PropertyValueFactory<article, String>("kodi1"));
		pershkrimi.setCellValueFactory(new PropertyValueFactory<article, String>("pershkrimi"));
		njesi.setCellValueFactory(new PropertyValueFactory<article, String>("njesia"));
		min.setCellValueFactory(new PropertyValueFactory<article, Integer>("min"));
		cmimi.setCellValueFactory(new PropertyValueFactory<article, Double>("pricein"));
		gjendje.setCellValueFactory(new PropertyValueFactory<article, Double>("quantity"));
		
		table.setItems(getArticles());
		
		
	}

}
