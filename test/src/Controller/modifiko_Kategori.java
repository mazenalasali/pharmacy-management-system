package Controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

import org.controlsfx.dialog.Dialogs;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class modifiko_Kategori implements Initializable{

	Database db = new Database();
	Service service = new Service();
	
	@FXML
	private ComboBox combo;
	
	@FXML
	private CheckBox status;
	
	@FXML
	private TextField emri,id;
	
	@FXML
	private TextArea persh;
	
	@FXML
	private Button ruaj,pastro,back;
	
	public void PastroAction(ActionEvent event)
	{
		emri.setText("");
		status.setSelected(true);
		persh.setText("");
		
	}
	@FXML
	public void BackAction(ActionEvent event)
	{
		Stage stage = (Stage) back.getScene().getWindow();
		
		stage.close();
	}
	@FXML
	private void RuajAction(ActionEvent event)
	{
		Connection conn = db.connect();
		Statement smt;
		String sts;
		if(emri.getText().isEmpty())
		{
			ruaj.disabledProperty();
			Dialogs.create()
			.title("MESAZHI")
			.message("Ju lutem plotesojini te gjithe fushat !!! ")
			.styleClass(org.controlsfx.dialog.Dialog.STYLE_CLASS_NATIVE)
			.showWarning();
			return;
		}
		
		if(status.isSelected())
			sts = "AKTIV";
		else
			sts = "PASIV";
		
		String sql = "UPDATE `categories` SET `name`='"+emri.getText()+"',`status`='"+sts+"',`description`='"+persh.getText()+"' WHERE id='"+id.getText()+"'";

		try {
			smt = conn.createStatement();
			smt.executeUpdate(sql);
			
			Dialogs.create()
			.title("MESAZHI")
			.message("Kategoria u modifikua me sukses !!! ")
			.styleClass(org.controlsfx.dialog.Dialog.STYLE_CLASS_NATIVE)
			.showInformation();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Stage stage = (Stage) ruaj.getScene().getWindow();
		stage.close();
	}
	
	@FXML
	public void MouseEvent(Event event)
	{
		String selected = combo.getSelectionModel().getSelectedItem().toString();
		int kat_id = service.getkategori(selected);
		String sql = "select * from categories where id='"+kat_id+"'";
		Connection conn = db.connect();
		Statement st = null;
		ResultSet rs = null;
		
		
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			
			if(rs.next())
			{
				id.setText(Integer.toString(kat_id));
				emri.setText(rs.getString(2));
				if(rs.getString(3).equals("AKTIV"))
					status.setSelected(true);
				else 
					status.setSelected(false);
				
				persh.setText(rs.getString(4));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		ResultSet rs = service.getcategories();
		ObservableList<String> list = FXCollections.observableArrayList();
		try {
			while(rs.next())
			{
				list.add(rs.getString(2));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		{
			combo.setItems(list);
			
		}
		id.setEditable(false);
		
		
	}

}
