package Controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

import Model.article;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class magazinaController implements Initializable{

	Database db = new Database();
	
	@FXML
	private TableView<article> table;
	
	@FXML
	private TableColumn<article, Integer> id;
	
	@FXML
	private TableColumn<article, String> kodi1;
	
	@FXML
	private TableColumn<article, String> kodi2;
	
	@FXML
	private TableColumn<article, String> produkti;
	
	@FXML
	private TableColumn<article, Integer> sasia;
	
	@FXML
	private TableColumn<article, String> njesia;
	
	@FXML
	private TableColumn<article, Double> shitje;
	
	@FXML
	private TableColumn<article, Double> blerje;
	
	public ObservableList<article> getArticles()
	{
		ObservableList<article> articles = FXCollections.observableArrayList();
		Connection conn = db.connect();
		Statement st = null;
		ResultSet rs = null;
		article a;
		String kateg = "Jo Kategori";
		
		String sql = "select * from articles";
		
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			
			while(rs.next())
			{
				kateg = "";
				System.out.println(kateg);
				a = new article(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getInt(6), kateg, rs.getString(8),rs.getInt(9),rs.getString(10),rs.getDouble(11),rs.getDouble(12));
				articles.add(a);
				System.out.println(a);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return articles;
	}
	
	
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		id.setCellValueFactory(new PropertyValueFactory<article, Integer>("id"));
		kodi1.setCellValueFactory(new PropertyValueFactory<article, String>("kodi1"));
		kodi2.setCellValueFactory(new PropertyValueFactory<article, String>("kodi2"));
		produkti.setCellValueFactory(new PropertyValueFactory<article, String>("pershkrimi"));
		sasia.setCellValueFactory(new PropertyValueFactory<article, Integer>("quantity"));
		njesia.setCellValueFactory(new PropertyValueFactory<article, String>("njesia"));
		blerje.setCellValueFactory(new PropertyValueFactory<article, Double>("pricein"));
		shitje.setCellValueFactory(new PropertyValueFactory<article, Double>("priceout"));
		
		table.setItems(getArticles());
		
	}

}
