package Controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

import org.controlsfx.dialog.Dialogs;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class change_password implements Initializable{
	
	Database db = new Database();
	
	@FXML
	private PasswordField oldpass,newpass,repass;
	
	@FXML
	private Button ok;
	
	@FXML
	private Label welcome;
	
	private int id_user;
	
	
	
	
	
	public void getUser(int id, String lbl)
	{
		System.out.println(id + " " + lbl);
		welcome.setText(lbl);
		id_user = id;
	}
	
	public String findUser()
	{
		Connection conn = db.connect();
		ResultSet rs = null;
		Statement st = null;
		String sql = "select * from users where id='"+id_user+"'";
		String value = null;
		
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			
			if(rs.next())
			{
				value = rs.getString(10);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return value;
	}
	
	@FXML
	public void UpdatePasswordAction(ActionEvent event)
	{
		
		Statement smt = null;
		Connection conn = db.connect();
		
		 if(oldpass.getText().isEmpty() ||newpass.getText().isEmpty() || repass.getText().isEmpty())
			{
			 	ok.disabledProperty();
			 	Dialogs.create()
				.title("MESAZHI")
				.message("Ju lutem plotesojini te gjithe fushat !!! ")
				.styleClass(org.controlsfx.dialog.Dialog.STYLE_CLASS_NATIVE)
				.showWarning();
				return;
			}
		
		String value = findUser();
		
		System.out.println(value);
		if(oldpass.getText().equals(value)){
			if(newpass.getText().equals(repass.getText())){
			String sql = "update users set password='"+ newpass.getText() + "' where id='"+ id_user + "'";
				try {
					smt = conn.createStatement();
					smt.executeUpdate(sql);
					
					Dialogs.create()
					.title("MESAZHI")
					.message("Fjalekalimi u ndryshua me sukse !!! ")
					.styleClass(org.controlsfx.dialog.Dialog.STYLE_CLASS_NATIVE)
					.showInformation();
					close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else
			{
				Dialogs.create()
				.title("MESAZHI")
				.message("Ju lutem shkruani passwordin e njejte !!! ")
				.styleClass(org.controlsfx.dialog.Dialog.STYLE_CLASS_NATIVE)
				.showWarning();
			}
		}
		else
		{
			Dialogs.create()
			.title("MESAZHI")
			.message("Fjalekalimi i vjeter nuk eshte i sakte !!! ")
			.styleClass(org.controlsfx.dialog.Dialog.STYLE_CLASS_NATIVE)
			.showWarning();
		}
	}
	private void close()
	{
		if(ok.isPressed()){
		Stage stage = (Stage) ok.getScene().getWindow();
		
		stage.close();
		}
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}

}
