package Controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.ResourceBundle;

import org.controlsfx.dialog.Dialogs;

import Model.client;
import Model.worker;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class modifiko_Punetore implements Initializable{

	Database db = new Database();
	Service service = new Service();
	
	@FXML
	private TableView <worker> table;
	
	@FXML
	private TableColumn <worker, Integer> id;
	@FXML
	private TableColumn <worker, String> emri;
	@FXML
	private TableColumn <worker, String> mbiemri;
	@FXML
	private TableColumn <worker, String> address;
	@FXML
	private TableColumn <worker, Date> datelindja;
	@FXML
	private TableColumn <worker, String> email;
	@FXML
	private TableColumn <worker, Integer> telefoni;
	@FXML
	private TableColumn <worker, String> status;
	@FXML
	private TableColumn <worker, String> username;
	@FXML
	private TableColumn <worker, String> tipi;
	@FXML
	private TableColumn <worker, String> shenime;
	@FXML
	private TableColumn <worker, Date> date_regj;
	
	@FXML
	private TextField em,mb,ad,ema,te,us,ti;
	
	@FXML
	private TextArea pe;
	@FXML
	private DatePicker dob;
	@FXML
	private CheckBox st;
	@FXML
	private Button ruaj, pastro, back;
	@FXML
	private Label label;
	
	public ObservableList<worker> getClients()
	{
		ObservableList<worker> worker = FXCollections.observableArrayList();
		Connection conn = db.connect();
		Statement st = null;
		ResultSet rs = null;
		worker w;
		
		
		String sql = "select * from users";
		
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			
			while(rs.next())
			{
				
				String num_tel = rs.getBigDecimal(7).toString();
				DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
				String dt = df.format(rs.getDate(5));
				w = new worker(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4), dt,rs.getString(6),num_tel, rs.getString(8), rs.getString(9),rs.getString(11),rs.getString(12), rs.getDate(13));
				worker.add(w);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return worker;
	}
	public void PastroAction(ActionEvent event)
	{
		em.setText("");
		mb.setText("");
		ad.setText("");
		ema.setText("");
		te.setText("");
		us.setText("");
		ti.setText("");
		pe.setText("");
		LocalDate ld = LocalDate.now();
		dob.setValue(ld);
	}
	@FXML
	public void BackAction(ActionEvent event)
	{
		Stage stage = (Stage) back.getScene().getWindow();
		
		stage.close();
	}
	@FXML
	private void RuajAction(ActionEvent event)
	{
		Connection conn = db.connect();
		Statement smt;
		String sts;
		if(em.getText().isEmpty() ||mb.getText().isEmpty() || !st.isSelected() ||  us.getText().isEmpty() || ti.getText().isEmpty())
		{
		 ruaj.disabledProperty();
		 Dialogs.create()
			.title("MESAZHI")
			.message("Ju lutem plotesojini te gjithe fushat !!! ")
			.styleClass(org.controlsfx.dialog.Dialog.STYLE_CLASS_NATIVE)
			.showWarning();
			return;
		}
		if(st.isSelected())
			sts = "AKTIV";
		else
			sts = "PASIV";
		
		String sql = "UPDATE `users` SET `name`='"+em.getText()+"',`surname`='"+mb.getText()+"',`address`='"+ad.getText()+"',`dob`='"+dob.getValue()+"',`email`='"+ema.getText()+"',`phone_num`='"+te.getText()+ "',`status`='"+sts+"',`username`='"+us.getText()+ "',`type`='"+ti.getText()+ "',`description`='"+pe.getText()+ "' WHERE id='"+label.getText()+"'";
		
		try {
			smt = conn.createStatement();
			smt.executeUpdate(sql);
			
			Dialogs.create()
			.title("MESAZHI")
			.message("Punetori u modifikua me sukses !!! ")
			.styleClass(org.controlsfx.dialog.Dialog.STYLE_CLASS_NATIVE)
			.showWarning();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		table.getItems().clear();
		table.setItems(getClients());
		
	}
	
	@FXML
	public void MouseEvent(javafx.scene.input.MouseEvent event)
	{
		if(event.isPrimaryButtonDown() && event.getClickCount() == 2){
		worker w = (worker) table.getSelectionModel().getSelectedItem();
		em.setText(w.getName());
		mb.setText(w.getSurname());
		ad.setText(w.getAddress());
		ema.setText(w.getEmail());
		if(w.getStatus().equals("AKTIV"))
			st.setSelected(true);
		else 
			st.setSelected(false);

		LocalDate ld = LocalDate.now();
		dob.setValue(ld);
		te.setText(w.getNr_cel());
		us.setText(w.getUsername());
		ti.setText(w.getType());
		pe.setText(w.getShenime());
		
		label.setText(Integer.toString(w.getId()));	
		}
	
		
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		id.setCellValueFactory(new PropertyValueFactory<worker, Integer>("id"));
		emri.setCellValueFactory(new PropertyValueFactory<worker, String>("name"));
		mbiemri.setCellValueFactory(new PropertyValueFactory<worker, String>("surname"));
		address.setCellValueFactory(new PropertyValueFactory<worker, String>("address"));
		datelindja.setCellValueFactory(new PropertyValueFactory<worker, Date>("date"));
		email.setCellValueFactory(new PropertyValueFactory<worker, String>("email"));
		telefoni.setCellValueFactory(new PropertyValueFactory<worker, Integer>("nr_cel"));
		status.setCellValueFactory(new PropertyValueFactory<worker, String>("status"));
		username.setCellValueFactory(new PropertyValueFactory<worker, String>("username"));
		tipi.setCellValueFactory(new PropertyValueFactory<worker, String>("type"));
		shenime.setCellValueFactory(new PropertyValueFactory<worker, String>("shenime"));
		date_regj.setCellValueFactory(new PropertyValueFactory<worker, Date>("created_at"));
		
		table.setItems(getClients());
		
		te.addEventFilter(KeyEvent.KEY_TYPED, service.numeric_Validation(15));
		ti.addEventFilter(KeyEvent.KEY_TYPED, service.letter_Validation(6));
		em.addEventFilter(KeyEvent.KEY_TYPED, service.letter_Validation(30));
		mb.addEventFilter(KeyEvent.KEY_TYPED, service.letter_Validation(30));
		
	}

}
