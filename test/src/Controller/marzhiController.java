package Controller;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;

import Model.article;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

public class marzhiController implements Initializable{

	Database db = new Database();
	NumberFormat formatter = new DecimalFormat("#0.00");    
	
	@FXML
	private TableView<article> table;
	
	@FXML
	private TableColumn<article, Integer> id;
	
	@FXML
	private TableColumn<article, String> kodi;
	
	@FXML
	private TableColumn<article, String> produkti;
	
	@FXML
	private TableColumn<article, Integer> sasia;
	
	@FXML
	private TableColumn<article, String> njesia;
	
	@FXML
	private TableColumn<article, Double> shitje;
	
	@FXML
	private TableColumn<article, Double> blerje;
	@FXML
	private TableColumn<article, Double> marzhi;
	@FXML
	private TextField totali;
	
	private double marzhi_fitimi;
	
	public ObservableList<article> getArticles()
	{
		ObservableList<article> articles = FXCollections.observableArrayList();
		Connection conn = db.connect();
		Statement st = null;
		ResultSet rs = null;
		article a;
		String kateg = "Jo Kategori";
		
		String sql = "select * from articles where status='AKTIV'";
		
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			
			while(rs.next())
			{
				kateg = "";
				System.out.println(kateg);
				a = new article(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getInt(6), kateg, rs.getString(8),rs.getInt(9),rs.getString(10),rs.getDouble(11),rs.getDouble(12));
				articles.add(a);
				marzhi_fitimi = (((a.getPriceout()-a.getPricein())/a.getPriceout()) * 100);
				 BigDecimal bd = new BigDecimal(marzhi_fitimi);
				 bd = bd.setScale(2, RoundingMode.HALF_UP);
				 marzhi_fitimi = bd.doubleValue();
				a.setMarzhi(marzhi_fitimi);
				System.out.println(marzhi_fitimi);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return articles;
	}
	
	
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		totali.setEditable(false);
		id.setCellValueFactory(new PropertyValueFactory<article, Integer>("id"));
		kodi.setCellValueFactory(new PropertyValueFactory<article, String>("kodi1"));
		produkti.setCellValueFactory(new PropertyValueFactory<article, String>("pershkrimi"));
		sasia.setCellValueFactory(new PropertyValueFactory<article, Integer>("quantity"));
		njesia.setCellValueFactory(new PropertyValueFactory<article, String>("njesia"));
		blerje.setCellValueFactory(new PropertyValueFactory<article, Double>("pricein"));
		shitje.setCellValueFactory(new PropertyValueFactory<article, Double>("priceout"));
		marzhi.setCellValueFactory(new PropertyValueFactory<article, Double>("marzhi"));
		
		table.setItems(getArticles());
		double tot = 0.0;
		ObservableList<article> articles = FXCollections.observableArrayList();
		ArrayList<Double> columnData = new ArrayList<Double>(table.getItems().size());
		for(article item : table.getItems())
		{
			columnData.add(marzhi.getCellObservableValue(item).getValue());
			
		}
		for(int i = 0; i < columnData.size(); i++)
		{
			tot += columnData.get(i);
		}
		tot = tot / columnData.size();
		totali.setText(String.format( "%.2f", tot));
		
	}

}
