package Controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.ResourceBundle;

import org.controlsfx.dialog.Dialogs;

import Model.client;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class addKlientController implements Initializable{
	
	Database db = new Database();
	Service service = new Service();
	
	@FXML
	private TextField emri,mbiemri, add,email,tel;
	
	@FXML
	private CheckBox status;
	
	@FXML
	private ChoiceBox kat;
	
	@FXML
	private TextArea shenime;
	
	@FXML
	private Button Ruaj;
	
	@FXML
	private void PastroAction(ActionEvent event)
	{
		emri.setText("");
		mbiemri.setText("");
		add.setText("");
		email.setText("");
		tel.setText("");
		shenime.setText("");
	}
	
	@FXML
	private void RuajAction(ActionEvent event)
	{
		String sql = "INSERT INTO `clients`(`id`, `name`, `surname`, `address`, `email`,`phone_num`,`categori_id`, `description`, `status`)"+ " VALUES (?,?,?,?,?,?,?,?,?)";
		 Connection conn = db.connect();
		 if(emri.getText().isEmpty() ||mbiemri.getText().isEmpty())
			{
				Ruaj.disabledProperty();
				Dialogs.create()
				.title("MESAZHI")
				.message("Ju lutem plotesojini te gjithe fushat !!! ")
				.styleClass(org.controlsfx.dialog.Dialog.STYLE_CLASS_NATIVE)
				.showWarning();
				return;
			}
		 
		 String str = "";
		 int id = 0;
		 int kat_id = 1;
		 String ktg = "Jo Kategori";
		
			 if(kat.isPressed()){
			  ktg = kat.getSelectionModel().getSelectedItem().toString();
			  kat_id = service.getkategori(ktg);
			 System.out.println(kat_id);
			 }
			 if(status.isSelected())
			 {
				 str="AKTIV";
			 }
			 else 
				 str = "PASIV";
			 
			 
			 client c = new client(id,emri.getText(),mbiemri.getText(), add.getText(),email.getText(),tel.getText(), str,ktg, shenime.getText(),new Date());
			 
			 try {
				PreparedStatement pst = (PreparedStatement)conn.prepareStatement(sql);
				
				pst.setInt(1, c.getId());
				pst.setString(2, c.getName());
				pst.setString(3, c.getSurname());
				pst.setString(4, c.getAddress());
				pst.setString(5, c.getEmail());
				pst.setString(6, c.getNr_cel());
				pst.setInt(7, kat_id);
				pst.setString(8, c.getPershkrimi());
				pst.setString(9, c.getStatus());
				
				pst.execute();
				 pst.close();
				 conn.close();
				 Dialogs.create()
					.title("MESAZHI")
					.message("Klienti u ruajt me sukse !!! ")
					.styleClass(org.controlsfx.dialog.Dialog.STYLE_CLASS_NATIVE)
					.showInformation();
				 close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 
		 //close();
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		ResultSet rs = service.getcategories();
		ObservableList<String> list = FXCollections.observableArrayList();
		try {
			while(rs.next())
			{
				list.add(rs.getString(2));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		kat.setItems(list);
		
		tel.addEventFilter(KeyEvent.KEY_TYPED, service.numeric_Validation(10));
		
	}
	
	private void close()
	{
		Stage stage = (Stage) Ruaj.getScene().getWindow();
		
		stage.close();
	}
	
	
}
