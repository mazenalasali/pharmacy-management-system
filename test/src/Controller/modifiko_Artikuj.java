package Controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

import org.controlsfx.dialog.Dialogs;

import Model.article;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class modifiko_Artikuj implements Initializable{

	Database db = new Database();
	Service service = new Service();
	
	@FXML
	private TableView table;
	
	@FXML
	private TableColumn <article,Integer> id ;
	@FXML
	private TableColumn <article,String> kodi1;
	@FXML
	private TableColumn <article,String> kodi2;
	@FXML
	private TableColumn <article,String> pershkrimi;
	@FXML
	private TableColumn <article,String> njesi;
	@FXML
	private TableColumn <article,Integer> sasi;
	@FXML
	private TableColumn <article,Integer> min;
	@FXML
	private TableColumn <article,String> kate;
	@FXML
	private TableColumn <article,String> kompesim;
	@FXML
	private TableColumn <article,String> sts;
	@FXML
	private TableColumn <article,Double> cmimi_shitje;
	@FXML
	private TableColumn <article,Double> cmimi_blerje;
	
	
	@FXML
	private TextField kod1, kod2, persh, minimum, shitje,article_id;
	
	@FXML
	private CheckBox status, komp;
	
	@FXML
	private ChoiceBox njesia, kategoria;
	
	@FXML
	private Button ruaj, pastro, back;
	
	
	public ObservableList<article> getArticles()
	{
		ObservableList<article> articles = FXCollections.observableArrayList();
		Connection conn = db.connect();
		Statement st = null;
		ResultSet rs = null;
		article a;
		String kateg = "Jo Kategori";
		System.out.println(conn);
		String sql = "select * from articles";
		
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			
			while(rs.next())
			{
				kateg = service.getkategoria(rs.getInt(7));
				System.out.println(kateg);
				a = new article(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getInt(6), kateg, rs.getString(8),rs.getInt(9),rs.getString(10),rs.getDouble(11),rs.getDouble(12));
				articles.add(a);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return articles;
	}
	
	@FXML
	public void PastroAction(ActionEvent event)
	{
		kod1.setText("");
		kod2.setText("");
		persh.setText("");
		minimum.setText("");
		shitje.setText("");
		kategoria.getSelectionModel().select(0);
		njesia.getSelectionModel().selectFirst();
		article_id.setText("");
	}
	@FXML
	public void BackAction(ActionEvent event)
	{
		Stage stage = (Stage) back.getScene().getWindow();
		
		stage.close();
	}
	@FXML
	public void RuajAction(ActionEvent event)
	{
		
		Connection conn = db.connect();
		Statement smt;
		
		 if(kod1.getText().isEmpty() ||persh.getText().isEmpty() || njesia.getSelectionModel().isEmpty() || !komp.isSelected() || !status.isSelected() || shitje.getText().isEmpty())
			{
			 ruaj.disabledProperty();
			 Dialogs.create()
				.title("MESAZHI")
				.message("Ju lutem plotesojini te gjithe fushat !!! ")
				.styleClass(org.controlsfx.dialog.Dialog.STYLE_CLASS_NATIVE)
				.showWarning();
				return;
			}
		String nje = njesia.getSelectionModel().getSelectedItem().toString();
		String k = kategoria.getSelectionModel().getSelectedItem().toString();
		int kat_id = service.getkategori(k);
		String sts;
		String kompesim;
		if(status.isSelected())
			sts = "AKTIV";
		else
			sts = "PASIV";
		
		if(komp.isSelected())
			kompesim = "PO";
		else
			kompesim = "JO";
	
		String sql = "UPDATE `articles` SET `kodi_1`='"+kod1.getText()+ "',`kodi_2`='"+kod2.getText() +"',`description`='"+persh.getText() + "',`Njesia`='"+nje+"',`min`='"+minimum.getText()+"',`kategori_id`='"+kat_id +"',`kompesim`='"+kompesim+"',`status`='"+sts+"',`price_out`='"+shitje.getText()+"' WHERE id='"+article_id.getText()+"';";
		
		
		try {
			smt = conn.createStatement();
			smt.executeUpdate(sql);
			
			Dialogs.create()
			.title("MESAZHI")
			.message("Artikulli u modifikua me sukses !!! ")
			.styleClass(org.controlsfx.dialog.Dialog.STYLE_CLASS_NATIVE)
			.showInformation();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		table.getItems().clear();
		table.setItems(getArticles());
	}
	
	
	
	@FXML
	public void MouseEvent(javafx.scene.input.MouseEvent event)
	{
		if(event.isPrimaryButtonDown() && event.getClickCount() == 2){
		article art = (article) table.getSelectionModel().getSelectedItem();
		kod1.setText(art.getKodi1());
		kod2.setText(art.getKodi2());
		persh.setText(art.getPershkrimi());
		njesia.getSelectionModel().select(art.getNjesia());
		kategoria.getSelectionModel().select(art.getKategoria());
		minimum.setText(Integer.toString(art.getMin()));
		if(art.getStatus().equals("AKTIV"))
			status.setSelected(true);
		else
			status.setSelected(false);
		if(art.getKompesim().equals("PO"))
			komp.setSelected(true);
		else
			komp.setSelected(false);
		
		shitje.setText(Double.toString(art.getPriceout()));
		article_id.setText(Integer.toString(art.getId()));
		//System.out.println(table.getSelectionModel().getSelectedIndex());
		}
		
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		id.setCellValueFactory(new PropertyValueFactory<article, Integer>("id"));
		kodi1.setCellValueFactory(new PropertyValueFactory<article, String>("kodi1"));
		kodi2.setCellValueFactory(new PropertyValueFactory<article, String>("kodi2"));
		pershkrimi.setCellValueFactory(new PropertyValueFactory<article, String>("pershkrimi"));
		njesi.setCellValueFactory(new PropertyValueFactory<article, String>("njesia"));
		sasi.setCellValueFactory(new PropertyValueFactory<article, Integer>("quantity"));
		min.setCellValueFactory(new PropertyValueFactory<article, Integer>("min"));
		kate.setCellValueFactory(new PropertyValueFactory<article, String>("kategoria"));
		kompesim.setCellValueFactory(new PropertyValueFactory<article, String>("kompesim"));
		sts.setCellValueFactory(new PropertyValueFactory<article, String>("status"));
		cmimi_blerje.setCellValueFactory(new PropertyValueFactory<article, Double>("pricein"));
		cmimi_shitje.setCellValueFactory(new PropertyValueFactory<article, Double>("priceout"));
		
		table.setItems(getArticles());
		
		kod1.addEventFilter(KeyEvent.KEY_TYPED, service.numeric_Validation(20));
		kod2.addEventFilter(KeyEvent.KEY_TYPED, service.numeric_Validation(20));
		minimum.addEventFilter(KeyEvent.KEY_TYPED, service.numeric_Validation(3));
		shitje.addEventFilter(KeyEvent.KEY_TYPED, service.numeric_Validation(10));
		article_id.setEditable(false);
		
		ResultSet rs = service.getcategories();
		ObservableList<String> zgjedhje = FXCollections.observableArrayList();
		
		try {
			while(rs.next()){
			zgjedhje.add(rs.getString(2));
			kategoria.setItems(zgjedhje);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		njesia.setItems(service.getNjesia());
		
	}
	private void close()
	{
		if(back.isPressed()){
		Stage stage = (Stage) back.getScene().getWindow();
		
		stage.close();
		}
	}

}
